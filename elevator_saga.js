// TODO
// Distinguish between floor calls and passenger calls on an elevator's destination queue, dynamically re-distribute floor calls as necessary
// When an elevator stops at a floor, purge all non-passenger calls and re-distribute floor calls for all elevators

// Define "busyiness" as a combination of load factor and length of destination queue
// Route the least busy elevators to new calls, if the calls are not reasonably soon on the destination queue of another elevator

{
    init: function(elevators, floors) {
        var idleFloors = [];
        var fullLoad = .6;
        var idleQueue = {};

        ///////////////////////////////////////////////////////////
        // Handlers
        ///////////////////////////////////////////////////////////

        var handleIdle = function(elevator, elevators) {
            // If there's something in the idle queue, service the first one
            var idleArray = Object.keys(idleQueue);
            if (idleArray.length != 0) {
                var floorNum = idleArray.shift();
                elevator.goToFloor(floorNum);
                delete idleQueue[floorNum];
                console.log("Elevator id %d idle, servicing floor %s off idle queue, queue after delete: %s", 
                    elevator.index, floorNum, JSON.stringify(idleQueue));
            } else {
                // Otherwise nothing to do, return to idle floor
                var idleFloor = idleFloors[elevator.index];
                console.log("Elevator id %d returning to idle floor %d", elevator.index, idleFloor);
                elevator.goToFloor(idleFloor);
            }
        };

        var handleNewFloorButton = function(elevator, floorNum) {
            var queue = elevator.destinationQueue;
            // If this floor is already in the queue, we're fine, just leave it alone
            if (queue.indexOf(floorNum) > 0) {
                console.log("Elevator id %d ignoring request to go to floor %d because it's already in the queue", elevator.index, floorNum);
                return;
            }

            // If we're not full, assume we picked up everyone at this floor up and remove it from the idle queue
            if (elevator.currentFloor() in idleQueue) {
                console.log("Picked up passengers on floor %d, floor buttonStates: %s", elevator.currentFloor(), JSON.stringify(floors[floorNum].buttonStates));
                if (elevator.loadFactor() < fullLoad) {
                    console.log("Elevator id %d stopped at floor in idle queue and picked up all passengers. Removing %d from idle queue. New queue: %s", 
                        elevator.index, elevator.currentFloor(), JSON.stringify(idleQueue));
                    delete idleQueue[elevator.currentFloor()];
                } else {
                    console.log("Elevator id %d stopped at floor in idle queue %d and is full", elevator.index, elevator.currentFloor());
                }
            }

            // Add the floor to the queue
            console.log("Elevator id %d accepting request to go to floor %d", elevator.index, floorNum);
            elevator.goToFloor(floorNum);

            // Sort destination floors in order of closest to where the elevator is now
            elevator.destinationQueue = sortElevatorDestinationQueue(elevator);
            elevator.checkDestinationQueue();
        };

        var handleNewFloorCall = function(floorNum, direction, elevators) {
            console.log("Handling new floor call on floor %d heading %s", floorNum, direction);
            // Find the nearest elevator headed in that direction, or idle
            var nearestElevator = null;
            var nearestDistance = Number.MAX_SAFE_INTEGER;
            elevators.forEach(function(elevator, index) {
                var distance = getElevatorDistanceFromFloor(elevator, floorNum);

                if (elevator.loadFactor() >= fullLoad) {
                    console.log("Elevator id %d has loadFactor %f and can't service the call", index, elevator.loadFactor);
                }

                if (!isElevatorHeadingTowardFloor(elevator, floorNum)) {
                    console.log("Elevator id %d is not heading toward floor %d and can't service the call", index, floorNum);
                }

                if ((distance < nearestDistance) &&
                    isElevatorHeadingTowardFloor(elevator, floorNum) &&
                // Skip elevators that are full
                    elevator.loadFactor() < fullLoad) {

                    console.log("Elevator id %d is heading toward floor %d and is %d away, which is closer than %d", index, floorNum, distance, nearestDistance);

                    nearestDistance = distance;
                    nearestElevator = elevator;
                }
            });

            if (nearestElevator == null) {
                console.log("Could not find non-full elevator heading in the right direction to handle call at floor %d heading %s, adding call to idle queue", floorNum, direction);

                // Add call to idle queue
                idleQueue[floorNum] = true;

                // TODO: better sync between elevator schedules
                return;
            }

            console.log("Routing elevator id %d with load factor %f to handle %s call at floor %d", nearestElevator.index, nearestElevator.loadFactor(), direction, floorNum);
            // Add the floor to the elevator's queue
            nearestElevator.goToFloor(floorNum);

            // Re-sort queue
            nearestElevator.destinationQueue = sortElevatorDestinationQueue(nearestElevator);
            nearestElevator.checkDestinationQueue();
        };

        var handleStopAtFloor = function(elevator, floor) {
            var floorNum = floor.floorNum();
            var direction = getElevatorDirection(elevator);
            // Figure out which direction we're going and light up
            if (direction == 'up') {
                // If we're going up
                elevator.goingUpIndicator(true);
                elevator.goingDownIndicator(false);
                console.log("Elevator id %d stopped at floor %d, heading up", elevator.index, floorNum);
            } else if (direction == 'down') {
                // If we're going down
                elevator.goingUpIndicator(false);
                elevator.goingDownIndicator(true);
                console.log("Elevator id %d stopped at floor %d, heading down", elevator.index, floorNum);
            } else {
                // Otherwise we're idle and have no direction
                if (floor.buttonStates) {
                    var states = floor.buttonStates;
                    if (('up' in states) && states.up) {
                        // We're going up
                        elevator.goingUpIndicator(true);
                        elevator.goingDownIndicator(false);
                        console.log("Elevator id %d stopped at floor %d, accepting up button press", elevator.index, floorNum);
                    } else if (('down' in states) && states.down) {
                        // We're going down
                        elevator.goingUpIndicator(false);
                        elevator.goingDownIndicator(true);
                        console.log("Elevator id %d stopped at floor %d, accepting down button press", elevator.index, floorNum);
                    }
                } else {
                    console.log("Elevator id %d idle at floor %d, unable to read floor button states.", elevator.index, floorNum);
                }
            }
        };

        ///////////////////////////////////////////////////////////
        // Helpers
        ///////////////////////////////////////////////////////////

        var getElevatorDirection = function(elevator) {
            var queue = elevator.destinationQueue;
            // Figure out which direction we're going
            if (queue.length > 0) {
                var nextFloor = queue[0];
                if (nextFloor - elevator.currentFloor() > 0) {
                    console.log("Found direction up for elevator id %d", elevator.index);
                    return 'up';
                } else {
                    console.log("Found direction down for elevator id %d", elevator.index);
                    return 'down';
                }
            }
            console.log("Found direction idle for elevator id %d", elevator.index);
            return 'idle';
        };

        var getElevatorDistanceFromFloor = function(elevator, floorNum) {
            console.log("Finding distance of elevator id %d from floor %d...", elevator.index, floorNum);
            return Math.abs(elevator.currentFloor() - floorNum);
        };

        var isElevatorHeadingTowardFloor = function(elevator, floorNum) {
            var direction = getElevatorDirection(elevator);
            var relativePosition = getRelativePosition(elevator, floorNum);
            return (relativePosition == 'below' && direction == 'up') ||
                (relativePosition == 'above' && direction == 'down') ||
                direction == 'idle';
        };

        var getRelativePosition = function(elevator, floorNum) {
            return (elevator.currentFloor() - floorNum) > 0 ? 'above' : 'below';
        };

        var sortElevatorDestinationQueue = function(elevator) {
            console.log("Sorting destination queue for elevator id %d", elevator.index);
            // TODO: optimize this and make some magic happen here

            // Clone the queue so we can work on it
            var queue = elevator.destinationQueue.slice(0);

            // Find the floor in the queue we're closest to
            var closestFloorNum = null;
            var closestFloorDistance = Number.MAX_SAFE_INTEGER;
            queue.forEach(function (floorNum) {
                var distance = Math.abs(elevator.currentFloor() - floorNum);

                if (distance < closestFloorDistance) {
                    closestFloorNum = floorNum;
                    closestFloorDistance = distance;
                }
            })
            console.log("Sorting destination queue for elevator id %d, found closest floor %d at distance %d", elevator.index, closestFloorNum, closestFloorDistance);

            var position = getRelativePosition(elevator, closestFloorNum);
            if (position == 'below') {
                console.log("Sorting destination queue for elevator id %d, below closest floor, initial destination queue: %s", elevator.index, JSON.stringify(queue));
                // Slice from the closest floor to the end of the original queue
                // Put that at the beginning of the output queue
                var out = queue.splice(queue.indexOf(closestFloorNum), queue.length - 1);

                // Sort the rest of the original queue descending
                queue.sort(function(a,b) {return b-a});

                // Put that at the end of the output queue
                out = out.concat(queue);
                console.log("Sorting destination queue for elevator id %d, below closest floor, sorted destination queue: %s", elevator.index, JSON.stringify(out));
            } else if (position == 'above') {
                console.log("Sorting destination queue for elevator id %d, above closest floor, initial destination queue: %s", elevator.index, JSON.stringify(queue));
                // Slice from the beginning to the the closest floor of the original queue
                // Put that at the beginning of the output queue
                var out = queue.splice(0, queue.indexOf(closestFloorNum) + 1);

                // Sort the rest of the original queue ascending
                queue.sort(function(a,b) {return a-b});

                // Put that at the end of the output queue
                out = out.concat(queue);
                console.log("Sorting destination queue for elevator id %d, above closest floor, sorted destination queue: %s", elevator.index, JSON.stringify(out));
            }
            return out;
        };

        ///////////////////////////////////////////////////////////
        // Init function
        ///////////////////////////////////////////////////////////

        elevators.forEach(function(elevator, index) {
            console.log("Initializing elevator index " + index);
            elevator.index = index;
            elevator.on("idle", function() {
                handleIdle(elevator, elevators);
            });
            elevator.on("floor_button_pressed", function(floorNum) {
                handleNewFloorButton(elevator, floorNum);
            });
            elevator.on("stopped_at_floor", function(floorNum) {
                handleStopAtFloor(elevator, floors[floorNum]);
            });
            // Calculate idle floors
            var segmentSize = floors.length/elevators.length;
            var segmentTop = segmentSize * (index + 1);
            var segmentMid = segmentTop - (segmentSize/2);
            console.log("Segment mid %f for index %d", segmentMid, index);
            idleFloors[index] = Math.round(segmentMid);
            console.log("Initializing elevator index %d idle floor to %d", index, idleFloors[index]);
        });
        floors.forEach(function(floor) {
            floor.on("up_button_pressed", function() {
                handleNewFloorCall(floor.floorNum(), "up", elevators);
            });
            floor.on("down_button_pressed", function() {
                handleNewFloorCall(floor.floorNum(), "down", elevators);
            });
        });
    },
    update: function(dt, elevators, floors) {
        // We normally don't need to do anything here
    }
}